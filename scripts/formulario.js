const database = firebase.database();


function sololetras(e) {
  let error1 = document.getElementById('error1')
  let requerido = document.getElementById('required');
  let nombre = document.getElementById('nombre')
  key = e.keyCode || e.which;

  teclado = String.fromCharCode(key).toLowerCase();
  letras = 'abcdefghijklmnopqrstuvwxyz';
  especiales = '8-37-38-46-164';
  teclado_especial = false

  for(let i in especiales){

    if (key == especiales[i]) {
      teclado_especial = true
      break;
    }

    if (letras.indexOf(teclado) == -1 && !teclado_especial){
      error1.innerHTML = 'Solo letras (A-Z)';
      requerido.innerHTML =''
      error1.style="color: red; margin-top: 5px; font-size: 12px;"
      nombre.style.border = '1px solid red'
      
      return false
    }
  }
}


function sololeApellido(e) {
  let error2 = document.getElementById('error2')
  let requerido2 = document.getElementById('required2');
  
  key = e.keyCode || e.which;

  teclado = String.fromCharCode(key).toLowerCase();
  letras = 'abcdefghijklmnopqrstuvwxyz';
  especiales = '8-37-38-46-164';
  teclado_especial = false

  for(let i in especiales){

    if (key == especiales[i]) {
      teclado_especial = true
      break;
    }

    if (letras.indexOf(teclado) == -1 && !teclado_especial){
      error2.innerHTML = 'Solo letras (A-Z)';
      requerido2.innerHTML =''
      error2.style="color: red; margin-top: 5px; font-size: 12px;"
      apellido.style.border = '1px solid red'
      
      return false
    }
  }
}

function sololeContacto(e) {
  let error11 = document.getElementById('error11')
  let required7 = document.getElementById('required7');
  
  key = e.keyCode || e.which;

  teclado = String.fromCharCode(key).toLowerCase();
  letras = 'abcdefghijklmnopqrstuvwxyz';
  especiales = '8-37-38-46-164';
  teclado_especial = false

  for(let i in especiales){

    if (key == especiales[i]) {
      teclado_especial = true
      break;
    }

    if (letras.indexOf(teclado) == -1 && !teclado_especial){
      error11.innerHTML = 'Solo letras (A-Z)';
      required7.innerHTML =''
      error11.style="color: red; margin-top: 5px; font-size: 12px;"
      emergencia.style.border = '1px solid red'
      
      return false
    }
  }
}






function SoloNumero(evt){
  let telefono = document.getElementById('telefono')
  let error3 = document.getElementById('error3')

  if (window.Event) {
    keynum = evt.keyCode;
  }else{
    keynum = evt.which;
  }

  if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13) {
    return true;
  }else{
    telefono.style = "border: solid 1px red";
    error3.style="color: red; margin-top: 5px; font-size: 12px;"
    error3.innerHTML = 'Solo numero (0-9)';
    requerido6.innerHTML = ""
    return false;
  }
}

function SoloNumeroEmergencia(evt){
  let telefonoEm = document.getElementById('telefonoEm')
  let error4 = document.getElementById('error4')

  if (window.Event) {
    keynum = evt.keyCode;
  }else{
    keynum = evt.which;
  }

  if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13) {
    return true;
  }else{
    telefonoEm.style = "border: solid 1px red";
    error4.style="color: red; margin-top: 5px; font-size: 12px;"
    error4.innerHTML = 'Solo numero (0-9)';
    requerido8.innerHTML = ""
    return false;
  }
}


function SoloCedula(evt){
  if (window.Event) {
    keynum = evt.keyCode;
  }else{
    keynum = evt.which;
  }

  if ((keynum > 47 && keynum < 58) || keynum == 8 || keynum == 13) {
    return true;
  }else{
    return false;
  }
}





  

function validar() {

  let nombre = document.getElementById('nombre')
  let apellido = document.getElementById('apellido')
  let fecha = document.getElementById('fecha')
  let nacionalidad = document.getElementById('nacionalidad')
  let correo = document.getElementById('correo')
  let telefono = document.getElementById('telefono')
  let emergencia = document.getElementById('emergencia')
  let telefonoEm = document.getElementById('telefonoEm')
  let hiden =document.getElementById('hiden')
  let hiden2 =document.getElementById('hiden2')
  let cedula = document.getElementById('cedula')
  let cedula2 = document.getElementById('cedula2')
  let requerido = document.getElementById('required');
  let requerido2 = document.getElementById('required2');
  let requerido3 = document.getElementById('required3');
  let requerido4 = document.getElementById('required4');
  let requerido5 = document.getElementById('required5');
  let requerido6 = document.getElementById('required6');
  let requerido7 = document.getElementById('required7');
  let requerido8 = document.getElementById('required8');
  let requerido10 = document.getElementById('required10');
  let error1 = document.getElementById('error1');
  let error2 = document.getElementById('error2');
  let error3 = document.getElementById('error3')
  let error5 = document.getElementById('error5')
  let error6 = document.getElementById('error6')
  let error11 = document.getElementById('error11')
  let select1 = document.getElementById('select1')
  let select2 = document.getElementById('select2')
  let select3 = document.getElementById('select3')
  let select4 = document.getElementById('select4')
  let select5 = document.getElementById('select5')
  let select6 = document.getElementById('select6')
  let select7 = document.getElementById('select7')
  let expresionRegular1=/^([0-9]+){9}$/;
  let expresionRegular2=/\s/;
  let validEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;   

  let checkp1 = document.querySelector('input[name="che-1"]:checked')
  let checkp2 = document.querySelector('input[name="che-2"]:checked')
  let checkp3 = document.querySelector('input[name="che-3"]:checked')
  let checkp4 = document.querySelector('input[name="che-4"]:checked')
  let checkp5 = document.querySelector('input[name="che-5"]:checked')
  let checkp6 = document.querySelector('input[name="che-6"]:checked')
  let checkp7 = document.querySelector('input[name="che-7"]:checked')

  if (nombre.value == '') {
    requerido.innerHTML = "favor de llenar este campo"
    requerido.style="color: red; margin-top: 5px; font-size: 12px;"
    nombre.style = "border: solid 1px red";
    error1.innerHTML = ''
    nombre.focus()
    return false;
    
  }else{
    requerido.innerHTML = "Todo bien"
    requerido.style="color: #27D900; margin-top: 5px; font-size: 12px;"
    nombre.style = "border: solid 2px #27D900";
    error1.innerHTML = ''

  }

  if (apellido.value == '') {
    requerido2.innerHTML = "favor de llenar este campo"
    requerido2.style="color: red; margin-top: 5px; font-size: 12px;"
    apellido.style = "border: solid 1px red";
    apellido.focus()
    error2.innerHTML = ''
    return false;
    
  }else{
    requerido2.innerHTML = "Todo bien"
    requerido2.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
    apellido.style = "border: solid 2px  #27D900";
    error2.innerHTML = ''
  }

  if (fecha.value == '') {
    requerido3.innerHTML = "favor de llenar este campo"
    requerido3.style="color: red; margin-top: 5px; font-size: 12px;"
    fecha.style = "border: solid 1px red";
    fecha.focus()
    return false;
  }else{
    requerido3.innerHTML = "Todo bien"
    requerido3.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
    fecha.style = "border: solid 2px  #27D900";
  }

  if (nacionalidad.value == '') {
    requerido4.innerHTML = "favor de llenar este campo"
    requerido4.style="color: red; margin-top: 5px; font-size: 12px;"
    nacionalidad.style = "border: solid 1px red";
    nacionalidad.focus()
    return false;
    
  }else{
    nacionalidad.style = "border: solid 2px  #27D900";
    requerido4.innerHTML = "Todo bien"
    requerido4.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
  }

  if (correo.value == '') {
    requerido5.innerHTML = "favor de llenar este campo"
    requerido5.style="color: red; margin-top: 5px; font-size: 12px;"
    correo.style = "border: solid 1px red";
    correo.focus()   
    return false;
     
  }else if (!validEmail.test(correo.value)) {
    console.log('entre')
    correo.style = "border: solid 1px red";
    requerido5.innerHTML = "correo invalido"
    requerido5.style="color: red; margin-top: 5px; font-size: 12px;"

  }else{
    correo.style = "border: solid 1px #27D900";
    requerido5.innerHTML = "Todo bien"
    requerido5.style="color: #27D900; margin-top: 5px; font-size: 12px;"
    correo.style = "border: solid 2px #27D900";
  }

  if (telefono.value == '') {
    requerido6.innerHTML = "favor de llenar este campo"
    requerido6.style="color: red; margin-top: 5px; font-size: 12px;"
    telefono.style = "border: solid 1px red";
    telefono.focus()
    error3.innerHTML = '';
    return false;
    
  }else if(expresionRegular2.test(telefono.value)){
    requerido6.innerHTML = "Hay espacios en blanco"
    requerido6.style="color: red; margin-top: 5px; font-size: 12px;"
    telefono.style = "border: solid 1px red";
    error3.innerHTML = '';

  }else if(!expresionRegular1.test(telefono.value) || telefono.value.length < 10){

    requerido6.innerHTML = "telefono incorrecto"
    requerido6.style="color: red; margin-top: 5px; font-size: 12px;"
    telefono.style = "border: solid 1px red";
    error3.innerHTML = '';

  }else{

    telefono.style = "border: solid 2px  #27D900";
    requerido6.innerHTML = "Todo bien"
    requerido6.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
    error3.innerHTML = '';
  }

  if (emergencia.value == '') {
    requerido7.innerHTML = "favor de llenar este campo"
    requerido7.style="color: red; margin-top: 5px; font-size: 12px;"
    emergencia.style = "border: solid 1px red";
    emergencia.focus()
    error11.innerHTML = ''
    return false;
    
  }else{
    emergencia.style = "border: solid 2px  #27D900";
    requerido7.innerHTML = "Todo bien"
    requerido7.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
  }

  if (telefonoEm.value == '') {
    requerido8.innerHTML = "favor de llenar este campo"
    requerido8.style="color: red; margin-top: 5px; font-size: 12px;"
    telefonoEm.style = "border: solid 1px red";
    telefonoEm.focus()
    error11.innerHTML = ''
    return false;
    
  }else if(expresionRegular2.test(telefonoEm.value)){

    requerido8.innerHTML = "Hay espacios en blanco"
    requerido8.style="color: red; margin-top: 5px; font-size: 12px;"
    telefonoEm.style = "border: solid 1px red";
    error11.innerHTML = ''

  }else if(!expresionRegular1.test(telefonoEm.value) || telefonoEm.value.length < 10){

    requerido8.innerHTML = "telefono incorrecto"
    requerido8.style="color: red; margin-top: 5px; font-size: 12px;"
    telefonoEm.style = "border: solid 1px red";
    error11.innerHTML = ''
  }else{
    telefonoEm.style = "border: solid 2px  #27D900";
    requerido8.innerHTML = "Todo bien"
    requerido8.style="color:  #27D900; margin-top: 5px; font-size: 12px;"
    error11.innerHTML = ''
  }
  

  if (edad.value < 18){
      cedula2.value = ''
      hiden.style.display = 'block'
      hiden2.style.display = 'none'
      requerido10.innerHTML = "favor de llenar el campo cedula"
      requerido10.style ="color: red; margin-top: 5px; font-size: 12px;"
      edad.style = "border: solid 1px red";
      

      if (edad.value == '') {
        hiden.style.display = 'none'
        hiden2.style.display = 'none'
        edad.style = "border: solid 1px red";
        requerido10.innerHTML = "favor de elegir una opcion"
        requerido10.style ="color: red; margin-top: 5px; font-size: 12px;"
      }

      if (cedula.value == '') {
        cedula.style = "border: solid 1px red";
        error5.innerHTML = 'favor llenar este campo';
        error5.style ="color: red; margin-top: 5px; font-size: 12px;"
      }else if (cedula.value.length < 11 || cedula.value.length > 11) {
        error5.innerHTML = 'cedula incorreta';
        error5.style ="color: red; margin-top: 5px; font-size: 12px;"
        cedula.style = "border: solid 1px red";
      }else{
        cedula.style = "border: solid 2px  #27D900";
        edad.style = "border: solid 2px #27D900";
        requerido10.innerHTML = "Todo bien"
        requerido10.style ="color:  #27D900;  margin-top: 5px; font-size: 12px;"
        error5.innerHTML = 'Todo bien';
        error5.style ="color: #27D900; margin-top: 5px; font-size: 12px;"
      }

    }else if (edad.value >= 18) {
      cedula.value = ''
      hiden.style.display = 'none'
      hiden2.style.display = 'block'
      edad.style = "border: solid 1px red";
      requerido10.innerHTML = "favor llenar campo cedula"
      requerido10.style ="color: red; margin-top: 5px; font-size: 12px;"

      if (cedula2.value == '') {
        cedula2.style = "border: solid 1px red";
        error6.innerHTML = 'favor llenar este campo';
        error6.style ="color: red; margin-top: 5px; font-size: 12px;"
      }else if (cedula2.value.length < 11 || cedula2.value.length > 11 ) {
        error6.innerHTML = 'cedula incorreta';
        error6.style ="color: red; margin-top: 5px; font-size: 12px;"
        cedula2.style = "border: solid 1px red";

      }else{
        cedula2.style = "border: solid 2px #27D900";
        edad.style = "border: solid 2px  #27D900";
        requerido10.innerHTML = "Todo bien"
        requerido10.style ="color: #27D900; margin-top: 5px; font-size: 12px;"
        error6.innerHTML = 'Todo bien';
        error6.style ="color: #27D900; margin-top: 5px; font-size: 12px;"
      }
    }


    
  if (checkp1 == null) {
    select1.innerHTML = 'favor de elegir una opcion'
    select1.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select1.innerHTML = "Todo bien"
    select1.style="color: #27D900; margin-top: 5px; font-size: 12px;"    
    
  }

  if (checkp3 == null) {
    select3.innerHTML = 'favor de elegir una opcion'
    select3.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select3.innerHTML = "Todo bien"
    select3.style="color: #27D900; margin-top: 5px; font-size: 12px;"  
     
  }

  if (checkp2 == null) {
    select2.innerHTML = 'favor de elegir una opcion'
    select2.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select2.innerHTML = "Todo bien"
    select2.style="color: #27D900; margin-top: 5px; font-size: 12px;"   
    
  }

  if (checkp4 == null) {
    select4.innerHTML = 'favor de elegir una opcion'
    select4.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select4.innerHTML = "Todo bien"
    select4.style="color: #27D900; margin-top: 5px; font-size: 12px;"   
    
  }

  if (checkp5 == null) {
    select5.innerHTML = 'favor de elegir una opcion'
    select5.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select5.innerHTML = "Todo bien"
    select5.style="color: #27D900; margin-top: 5px; font-size: 12px;"   
    
  }

  if (checkp7 == null) {
    select7.innerHTML = 'favor de elegir una opcion'
    select7.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    select7.innerHTML = "Todo bien"
    select7.style="color: #27D900; margin-top: 5px; font-size: 12px;" 
  }

  if (checkp6 == null) {
    select6.innerHTML = 'favor de elegir una opcion'
    select6.style="color: red; margin-top: 5px; font-size: 12px;"
    return false;
  }else{
    console.log('entre')
    select6.innerHTML = "Todo bien"
    select6.style="color: #27D900; margin-top: 5px; font-size: 12px;" 
  }
}


function enviarDatos(){

  let nombre = document.getElementById('nombre')
  let apellido = document.getElementById('apellido')
  let fecha = document.getElementById('fecha')
  let nacionalidad = document.getElementById('nacionalidad')
  let correo = document.getElementById('correo')
  let telefono = document.getElementById('telefono')
  let emergencia = document.getElementById('emergencia')
  let telefonoEm = document.getElementById('telefonoEm')
  let cedula = document.getElementById('cedula')
  let cedula2 = document.getElementById('cedula2')
  let form = document.getElementById('form')

  let pregunta1 = document.getElementById('pregunta1').innerHTML
  let checkp1 = document.querySelector('input[name="che-1"]:checked')

  let pregunta2 = document.getElementById('pregunta2').innerHTML
  let checkp2 = document.querySelector('input[name="che-2"]:checked')

  let pregunta3 = document.getElementById('pregunta3').innerHTML
  let checkp3 = document.querySelector('input[name="che-3"]:checked')

  let pregunta4 = document.getElementById('pregunta4').innerHTML
  let checkp4 = document.querySelector('input[name="che-4"]:checked')

  let pregunta5 = document.getElementById('pregunta5').innerHTML
  let checkp5 = document.querySelector('input[name="che-5"]:checked')

  let pregunta6 = document.getElementById('pregunta6').innerHTML
  let checkp6 = document.querySelector('input[name="che-6"]:checked')

  let pregunta7 = document.getElementById('pregunta7').innerHTML
  let checkp7 = document.querySelector('input[name="che-7"]:checked')

  let data = {
    infcliente:{
      nombre:nombre.value,
      apellido:apellido.value,
      nacionalidad:nacionalidad.value,
      fecha:fecha.value,
      correo:correo.value,
      telefono:telefono.value,
      emergencia:emergencia.value,
      telefonoEm:telefonoEm.value,
      cedula: cedula.value, 
      edad: edad.value,
      cedula: cedula.value,
      cedula2: cedula2.value,
      saludSeguridad:[
        {
          pregunta1:pregunta1,
          checkp1:checkp1.value,
          cometario:comentario1.value         
        },
        {
          pregunta2:pregunta2,
          checkp2:checkp2.value,
          cometario:comentario2.value 
        },
        {
          pregunta3:pregunta3,
          checkp3:checkp3.value,
          cometario:comentario3.value 
        },
        {
          pregunta4:pregunta4,
          checkp4:checkp4.value,
        }
      ],
  
      responsabilidad:[
        {
          pregunta5:pregunta5,
          checkp5:checkp5.value,
        },
        {
          pregunta6:pregunta6,
          checkp6:checkp6.value
        },
        {
          pregunta7:pregunta7,
          checkp7:checkp7.value,
        },
      ]
    }
  }  
  
  database.ref('/usuarios/').push(data)

  form.reset() 

  nombre.style.border = ''
  required.innerHTML =''

  apellido.style.border = ''
  required2.innerHTML =''

  fecha.style.border = ''
  required3.innerHTML =''

  nacionalidad.style.border = ''
  required4.innerHTML =''

  correo.style.border = ''
  required5.innerHTML =''

  telefono.style.border = ''
  required6.innerHTML =''

  emergencia.style.border = ''
  required7.innerHTML =''

  telefonoEm.style.border = ''
  required8.innerHTML =''

  edad.style.border = ''
  required10.innerHTML =''

  cedula.style.border = ''
  error5.innerHTML =''

  cedula2.style.border = ''
  error6.innerHTML =''

  select1.innerHTML =''
  select2.innerHTML =''
  select3.innerHTML =''
  select4.innerHTML =''
  select5.innerHTML =''
  select6.innerHTML =''
  select7.innerHTML =''

  let container_envio = document.getElementById('container_envio');
  let titulo = document.getElementById('titulo');


  setTimeout(() => {
    titulo.style.display = 'none'
    container_envio.style.display = 'block'
  }, 1000);

  setTimeout(() => {
    titulo.style.display = 'block'
    titulo.style.justifyContent = 'center'
    container_envio.style.display = 'none'
  }, 5000);




}