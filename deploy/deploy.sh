#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="trolbis/pages/trolbis-landingpage"

echo "Deploying to ${DEPLOY_SERVER}"
scp -r ./* root@${DEPLOY_SERVER}:/var/www/${SERVER_FOLDER}/

echo "Finished copying the build files"